#include <type_traits>
#include <iostream>
#include <stdio.h>

typedef long uint_type;

/* clean
template<unsigned a, unsigned b, class T, class F, class Enable = void>
struct prod {
	static T eval (const F f) {
		static_assert(a < b, "invalid range for product index");

		return f(b) * prod<a, b-1, T, F>::eval(f);
	}
};
*/

template<unsigned a, unsigned b, class T, class F, class Enable = void>
struct prod {
	static T eval (const F f) {
		static_assert(a < b, "invalid range for product index");

		// this really sucks, as the product is needed as an argument to
		//  provide numerical stability but now the functor has a counter
		//	intuitive signiture and its hard to inline the function
		return f(b, prod<a, b-1, T, F>::eval(f));
	}
};

template<unsigned a, unsigned b, class T, class F>
struct prod<a, b, T, F, typename std::enable_if<a == b>::type> {
	static T eval (const F f) {
		return f(a, 1);
	}
};

//unsigned n and k
template<uint_type n, uint_type k>
struct binomial {
	static uint_type eval () {
		static_assert(n > k, "n must be greater than k");

		const auto lambda = [] (uint_type i, uint_type result) {
			return ((n-k+i)*result)/i;
		};
                
		return prod<1, k, uint_type, decltype(lambda)>::eval(lambda);
	}
};

//neat idea using lambda

/*
  //shorter version:)

  template<unsigned N, unsigned K> struct binomial_simple {static uint_type eval() {return (N-K+1)*binomial_simple<N,K-1>::eval()/K;}};
  template<unsigned N> struct binomial_simple<N,1> {static uint_type eval() {return N;}};
*/

int main () {
	uint_type result = binomial<11, 4>::eval();
	std::cout << result << std::endl;
}
