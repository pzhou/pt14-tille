#ifndef OPERATOR_HPP
#	define OPERATOR_HPP

struct Add {
	template <class T1, class T2>
	static auto apply (T1 l, T2 r) -> decltype(l+r) { return l + r; }

	static const char symbol = '+';
};

struct Multiply {
	template <class T1, class T2>
	static auto apply (T1 l, T2 r) -> decltype(l*r) { return l * r; }

	static const char symbol = '*';
};

struct Composition {
	template <class T1, class T2>
	static auto apply (T1 l, T2 r) -> decltype(l(r)) { return l(r); }

	static const char symbol = 'o';
};

template <class L, class R>
inline Expression<L, R, Multiply> operator* (const L l, const R r) {
	return Expression<L, R, Multiply>(l, r);
}

template <class L, class R>
inline Expression<L, R, Add> operator+ (const L l, const R r) {
	return Expression<L, R, Add>(l, r);
}

#endif