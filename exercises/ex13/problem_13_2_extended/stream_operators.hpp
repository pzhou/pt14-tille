#include <type_traits>
#include "iostream"

/* operator<< for expression, constant, variable */
template <class L, class R, class Op>
std::ostream& operator<<(std::ostream& stream, Expression<L, R, Op> const & expr) { 
	if (std::is_same<Op, Composition>::value)
		return stream << "(" << expr.getLeft() << '(' << expr.getRight() << "))";

 	return stream << "(" << expr.getLeft() << Op::symbol << expr.getRight() << ")";
}

template <class T>
std::ostream& operator<<(std::ostream& stream, Constant<T> const & c) { 
  return stream << c();
}

template <class T>
std::ostream& operator<<(std::ostream& stream, Variable<T> const & var) { 
  return stream << "x";
}