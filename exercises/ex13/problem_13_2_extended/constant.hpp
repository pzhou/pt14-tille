#ifndef CONSTANT_HPP
#	define CONSTANT_HPP

template <class T>
class Constant {
public:
	typedef T type;
	typedef Constant<T> derivative_type;

	constexpr Constant(T y) : value_(y) {}

	// extended representation to match Variable interface
	template <class V>
	T operator() (const V y) const {
		return value_;
	}

	// return value
	T operator() () const { return value_; }

	// pre: T(0) is the zero element
	constexpr T derivative() const {
		return T(0);
	}

	// pre: T2 is comparable to T
	template <class T2>
	bool operator== (Constant<T2> c) const{
		return c.value_ == value_;
	}
private:
	const T value_;
};

template <class T>
Constant<T> constant(const T x)
{
  return Constant<T>(x);
}

#endif