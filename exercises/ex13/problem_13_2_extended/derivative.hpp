#ifndef DERIVATIVE_HPP
#	define DERIVATIVE_HPP

#include <type_traits>
#include "expression.hpp"
#include "operators.hpp"
#include "variable.hpp"
#include "constant.hpp"

/*template <class T, class Enable = void>
struct Differentiator {};

template <class T>
Differentiator<T>::derivative_type derivative (T expr) {
	return Differentiator<T>::derivative(expr);
};*/

template <class T, class Enable = void>
struct Differentiator {
	typedef typename T::derivative_type derivative_type;
	static typename T::derivative_type derivative (T func) {
		return func.derivative();
	}
};
// d/dx(any + any)
template <class T>
struct Differentiator<
	T,
	typename std::enable_if<
		std::is_same<const Expression<typename T::left_type, typename T::right_type, typename T::operator_type>, const T>::value
		&& std::is_same<typename T::operator_type, Add>::value
	>::type >
{
	typedef Expression<
		typename Differentiator<typename T::left_type>::derivative_type,
		typename Differentiator<typename T::right_type>::derivative_type,
		Add> derivative_type;

	static derivative_type derivative (T expr) {
		return Differentiator<decltype(expr.getLeft())>::derivative(expr.getLeft()) + Differentiator<decltype(expr.getRight())>::derivative(expr.getRight());
	}
};
// d/dx(any * any)
template <class T>
struct Differentiator<
	T,
	typename std::enable_if<
		std::is_same<const Expression<typename T::left_type, typename T::right_type, typename T::operator_type>, const T>::value
		&& std::is_same<typename T::operator_type, Multiply>::value
	>::type >
{
	typedef Expression<
		Expression<typename Differentiator<typename T::left_type>::derivative_type, typename T::right_type, Multiply>,
		Expression<typename Differentiator<typename T::right_type>::derivative_type, typename T::left_type, Multiply>,
		Add> derivative_type;

	static derivative_type derivative (T expr) {
		return Differentiator<decltype(expr.getLeft())>::derivative(expr.getLeft()) * expr.getRight() + Differentiator<decltype(expr.getRight())>::derivative(expr.getRight()) * expr.getLeft();
	}
};
// d/dx(const)
template <class T>
struct Differentiator<
	T,
	typename std::enable_if<std::is_same<const Constant<typename T::type>, const T>::value>::type>
{
	typedef Constant<typename T::type> derivative_type;

	static derivative_type derivative (T expr) {
		return Constant<typename T::type>(0);
	}
};
// d/dx(variable)
template <class T>
struct Differentiator<
	T,
	typename std::enable_if<std::is_same<const Variable<typename T::type>, const T>::value>::type>
{
	typedef Constant<typename T::type> derivative_type;

	static derivative_type derivative (T expr) {
		return Constant<typename T::type>(1);
	}
};

// d/dx(function o any) = d/dx(any) * (d/dx(function) o any)
template <class T>
struct Differentiator<
	T,
	typename std::enable_if<
		std::is_same<const Expression<typename T::left_type, typename T::right_type, typename T::operator_type>, const T>::value
		&& std::is_same<typename T::operator_type, Composition>::value
	>::type>
{
	typedef Expression<
		typename Differentiator<typename T::right_type>::derivative_type,
		Expression<
			typename Differentiator<typename T::left_type>::derivative_type,
			typename T::right_type,
			Composition
		>,
		Multiply
	> derivative_type;

	static auto derivative (T expr) -> derivative_type {
		return Differentiator<typename T::right_type>::derivative(expr.getRight()) * Expression<decltype(Differentiator<typename T::left_type>::derivative(expr.getLeft())), typename T::right_type, Composition>(Differentiator<typename T::left_type>::derivative(expr.getLeft()), expr.getRight());
	}
};

#endif