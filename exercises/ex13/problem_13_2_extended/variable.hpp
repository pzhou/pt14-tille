#ifndef VARIABLE_HPP
	#define VARIABLE_HPP

#include "constant.hpp"

template <class T>
class Variable {
public:
	typedef T type;

	typedef Constant<T> derivative_type;

	template <class V>
	T operator() (const V y) const {
		return y;
	}

	// pre: T(1) is the identity element of multiplication
	constexpr Constant<T> derivative() const {
		return Constant<T>(1);
	}
};
#endif