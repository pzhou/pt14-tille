#include <type_traits>

/* +, * operator */
struct Add {
	template <class T1, class T2>
	static inline auto apply(T1 a, T2 b) -> decltype(a + b) { return a + b; }

	static const char symbol = '+';
};

struct Multiply {
	template <class T1, class T2>
	static inline auto apply(T1 a, T2 b) -> decltype(a*b) { return a * b; }

	static const char symbol = '*';
};

template <class T>
class Constant {
public:
	typedef T type;
	typedef Constant<T> derivative_type;

	constexpr Constant(T y) : value_(y) {}

	// extended representation to match Variable interface
	template <class V>
	T operator() (const V y) const {
		return value_;
	}

	// return value
	T operator() () const { return value_; }

	// pre: T(0) is the zero element
	constexpr T derivative() const {
		return T(0);
	}

	// pre: T2 is comparable to T
	template <class T2>
	bool operator== (Constant<T2> c) const{
		return c.value_ == value_;
	}
private:
	const T value_;
};

template <class T>
Constant<T> constant(const T x)
{
  return Constant<T>(x);
}

template <class T>
class Variable {
public:
	typedef T type;

	typedef Constant<T> derivative_type;

	template <class V>
	T operator() (const V y) const {
		return y;
	}

	// pre: T(1) is the identity element of multiplication
	constexpr Constant<T> derivative() const {
		return Constant<T>(1);
	}
};

// d/dx(function o expression) = d/dx(expression) * (d/ds(function(s)) o expression) | chain rule

/* Expression differental operator
 *  currently just supports one variable
 *  since multiple variable support would 
 *  require reference type for expression
 *  operands
 *         
 *  d/dx(expr+expr) -> d/dx(expr) + d/dx(expr)
 *  d/dx(c + expr || expr + c) -> d/dx(expr)
 *  d/dx(expr1 * expr2) -> d/dx(expr1) * expr2 + d/dx(expr2) * expr1
 *  d/dx(c * expr) -> c * d/dx(expr)*c
 *  d/dx(expr * c) -> c * d/dx(expr)
 */
template <class L, class R, class Op>
class Expression;

template <class L, class R, class Op, class Enable = void>
struct Differentiator {};

// d/dx(expression + expression) = d/dx(expression) + d/dx(expression)
template <class L, class R>
struct Differentiator<L, R, Add, typename std::enable_if<
		!std::is_same<Constant<typename L::type>, L>::value
		&& !std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef Expression<typename L::derivative_type, typename R::derivative_type, Add> derivative_type;

	static derivative_type derivative(L l , R r) {
		//std::cout << "expr + expr" << std::endl;
		return l.derivative() + r.derivative();
	};
};

//	this is an end node to surpress Constant(0)+Constant(0) blow up
//	 (otherwise endless loop -> incomplete type)
template <class L, class R>
struct Differentiator<L, R, Add, typename std::enable_if<
		std::is_same<Constant<typename L::type>, L>::value
		&& !std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef typename R::derivative_type derivative_type;

	static derivative_type derivative(const L l, const R r) {
		//std::cout << "c+c" << std::endl;
		return r.derivative();
	};
};

template <class L, class R>
struct Differentiator<L, R, Add, typename std::enable_if<
		!std::is_same<Constant<typename L::type>, L>::value
		&& std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef typename L::derivative_type derivative_type;

	static auto derivative(const L l, const R r) -> decltype(l.derivative()) {
		//std::cout << "c+c" << std::endl;
		return l.derivative();
	};
};

template <class L, class R>
struct Differentiator<L, R, Add, typename std::enable_if<
		std::is_same<Constant<typename L::type>, L>::value
		&& std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef Constant<int> derivative_type;

	static derivative_type derivative(const L l, const R r) {
		//std::cout << "c+c" << std::endl;
		return derivative_type(0);
	};
};


// d/dx(expression * expression)
template <class L, class R>
struct Differentiator<L, R, Multiply, typename std::enable_if<
		!std::is_same<Constant<typename L::type>, L>::value
		&& !std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef Expression<
		Expression<typename L::derivative_type, R, Multiply>,
		Expression<typename R::derivative_type, L, Multiply>,
		Add> derivative_type;

	static derivative_type derivative(L l, R r) {
		//std::cout << "expr * expr " << std::endl;
		return (l.derivative() * r) + (r.derivative() * l);
	};
};

template <class L, class R>
struct Differentiator<L, R, Multiply, typename std::enable_if<
		std::is_same<Constant<typename L::type>, L>::value
		&& !std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef Expression<L, typename R::derivative_type, Multiply> derivative_type;

	constexpr static derivative_type derivative(const L l, const R r) {
		//std::cout << "d/dx(c * expr = " << l*r << ") = " << std::endl;
		return l*r.derivative();
	};
};

template <class L, class R>
struct Differentiator<L, R, Multiply, typename std::enable_if<
		!std::is_same<Constant<typename L::type>, L>::value
		&& std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef Expression<R, typename L::derivative_type, Multiply> derivative_type;

	constexpr static derivative_type derivative(const L l, const R r) {
		//std::cout << "expr * c = " << l.derivative()*r << std::endl;
		return r*l.derivative();
	};
};

template <class L, class R>
struct Differentiator<L, R, Multiply, typename std::enable_if<
		std::is_same<Constant<typename L::type>, L>::value
		&& std::is_same<Constant<typename R::type>, R>::value>
	::type> {
	typedef Constant<typename L::type> derivative_type;

	constexpr static derivative_type derivative(const L l, const R r) {
		//std::cout << "a" << std::endl;
		return derivative_type(0);
	};
};

template <class L, class R, class Op>
class Expression {
private:
	L left_;
	R right_;
public:
	typedef Expression type;
	typedef L left_type;
	typedef R right_type;
	typedef decltype(Differentiator<L, R, Op>::derivative(left_, right_)) derivative_type;

	Expression (const L l, const R r) : left_(l), right_(r) {}

	derivative_type derivative() const {
		return Differentiator<L, R, Op>::derivative(left_, right_);
	}

	template <class T>
	T operator() (T x) const {
		return Op::apply(left_(x), right_(x));
	}

	const L getLeft () const { return left_; }

	const R getRight () const { return right_; }
};

template <class L, class R>
inline Expression<L, R, Multiply> operator* (const L l, const R r) {
	return Expression<L, R, Multiply>(l, r);
}

template <class L, class R>
inline Expression<L, R, Add> operator+ (const L l, const R r) {
	return Expression<L, R, Add>(l, r);
}

// todo: debug stream to show steps
/*int main () {
	Variable<int> x;
	Constant<int> c(4);
	asm("# bla");
	volatile auto result = (c+c*x*x*c).derivative()(9);
	//std::cout << "Expr:" << expr << std::endl;
	//std::cout << "Result:" << result << std::endl;
}*/