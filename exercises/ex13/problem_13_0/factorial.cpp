#include <iostream>
typedef long long uint_type;

/*template <uint_type a, uint_type tmp=1>
class factorial {
public:
	static uint_type eval () {
		return factorial<a-1, tmp*a>::eval();
	}
};

template <uint_type tmp>
class factorial<1, tmp> {
public:
	static uint_type eval () {
		return tmp;
	}
};*/

//constexpr should not be necessary here

template <uint_type a>
class factorial {
public:
	constexpr static uint_type eval () {
		return a*factorial<a-1>::eval();
	}
};

template <>
class factorial<1> {
public:
	constexpr static uint_type eval () {
		return 1;
	}
};

int main () {
	unsigned blub = factorial<6>::eval();
	std::cout << blub << std::endl;
}
