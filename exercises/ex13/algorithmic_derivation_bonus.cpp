/* Programming Techniques for Scientific Simulations, HS 2014
 * Week 12
 */
 
#include <iostream>
#include "algorithmic_derivation.hpp"
#include "stream_operators.hpp"

// expression: 5*x*(x+1)+72*x+38 = 5*x^2 + 77*x + 38
// derivative: 10*x+77

int main()
{
	Variable<int> x;
	auto expr = constant(4)*x*x;
	auto expr_deriv = expr.derivative();
	auto expr_second_deriv = expr_deriv.derivative();
	std::cout << "f(x) = " << expr << std::endl
			  << "f'(x) = " << expr_deriv << std::endl
			  << "f''(x) = " << expr_second_deriv << std::endl;
	std::cout << "f(8) = " << expr(8) << ", f''(8) = " << expr_deriv(8) << std::endl;

  	return 0;
}