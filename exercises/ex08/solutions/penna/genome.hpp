#ifndef GENOME_HPP
#define GENOME_HPP

#include <bitset>
#include <limits>
#include <random>

namespace Penna
{

extern std::mt19937 rng;

typedef unsigned age_type;

/**
 * Genome class. Each gene is represented by one bit.
 */
class Genome
{
public:
  /// Up to this size bitset is a lot faster
  static const age_type number_of_genes = 
    std::numeric_limits<unsigned long>::digits;
    
  static void set_mutation_rate( age_type );
    
  /// Default constructor: Initialize genes to all good.
  /// Genome() {};  // provided by the compiler

  /// is the gene of year n bad?
  bool is_bad( age_type ) const;
  /// Count number of bad genes in first n years.
  age_type count_bad( age_type ) const;
  /// mutate the genome by flipping of M genes
  void mutate();

private:
  /// Parameter M in Penna's paper
  static age_type mutation_rate_;
  std::bitset<number_of_genes> genes_;
};

} // end namespace Penna

#endif // !defined GENOME_HPP
