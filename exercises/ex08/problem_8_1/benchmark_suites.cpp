// notes
// 	vector, list: insert(iter, val) has return type iterator
//  set: insert(key) has return type std::pair<iter, bool> where bool specifies whether the element was
//			inserted or if it was already there

namespace Benchmark {
namespace Suite {

// we could use traits here to distinguish the container types
// 	(SequenceContainer: std::vector, std::list...; AssociativeContrainer: std::set ...)

template <class V, template <class U, class Allocator = std::allocator<U>> class TContainer>
struct StdSequenceContainer {
	typedef V value_t;

	TContainer<V> container;

	StdSequenceContainer(V* it_begin, V* it_end) 
		: container(it_begin, it_end) {};

	void benchmark (const V& j) {
		// find first element greater than j
		//	note: actuall type is TContainer<V>::iterator
		auto predecessor = std::find_if(container.begin(), container.end(), [j] (const V& val) { return j < val; });

                //the initial point was for list and vector just to
                //insert a new element at a random position. to make
                //this generic, we can use std::advance to move the
                //container.begin() iterator forward by a random
                //number of steps. std::find_if might give similar
                //timings depending on the elements in the container
		
		auto it = container.insert(predecessor, j); // insert element, note: actuall type is TContainer<V>::iterator
		container.erase(it);
	}
};

template <class V, template <class U, class Compare = std::less<V>, class Allocator = std::allocator<U>> class TContainer>
struct StdAssociativeContainer {
	typedef V value_t;

	TContainer<V> container;

	StdAssociativeContainer(V* it_begin, V* it_end) 
		: container(it_begin, it_end) {};

	void benchmark (const V& j) {
		// find first element greater than j
		//	note: actuall type is pair<TContainer<V>::iterator, bool>
		auto result = container.insert(j); // insert element

		if (result.second)
			container.erase(result.first); // erase it
	}

};

};
};
