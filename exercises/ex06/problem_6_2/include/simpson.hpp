//#include "traits.hpp" - include before starting namespace

namespace simpson {
	#include "traits.hpp"

	/**
	 * Simpson integration step
	 *  see http://de.wikipedia.org/wiki/Simpsonregel
	 */
	template <typename F, typename domain_type>
	inline double integration_step (F f, domain_type x, domain_type delta_x) {
		return (f(x) + 4*f(x+delta_x/2) + f(x+delta_x));
	}//templated function should be automatically inlined

	/**
	 * Simpson integration
	 * @var f pointer to the function to integrate
	 * @var a lower bound
	 * @var b upper bound
	 * @var N number of steps 
	 */
	template <class F> using domain_type = typename simpson::traits::domain_type<F>::type;
	template <class F> using result_type = typename simpson::traits::result_type<F>::type;

	template <class F>
	result_type<F> integrate (
			const domain_type<F> a,
			const domain_type<F> b,
			const int N, const F& f) {

		const domain_type<F> delta_x = (b-a)/N;
		result_type<F> result(0.0);

		for (int i=0; i < N; i++) {
			result += integration_step(f, i*delta_x, delta_x);	// this is actuelly numerically not so wise but niceer to read
		}

		return (delta_x/6)*result;
	}
}
