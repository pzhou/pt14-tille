#include <iostream>
#include "simpson.hpp"

double f(double x) {
	return x*(1-x);
}

struct F {
  typedef double argument_type;
  typedef double result_type;
  
  inline result_type operator()(const argument_type x) const { return x*(1-x); }
};

int main () {
	F func;

	std::cout.precision(15);
	std::cout << "int_a^b exp(-lambda*x) dx  = " << simpson::integrate(0, 1, 10, func) << "  (function object version)" << std::endl;
        // std::cout << "int_a^b exp(-lambda*x) dx  = " << simpson::integrate(0, 1, 10, F()) << "  (function object version)" << std::endl; //construct temporary on the fly
	std::cout << "int_a^b exp(-lambda*x) dx  = " << simpson::integrate(0, 1, 10, f) << "  (function pointer version)" << std::endl;
}
