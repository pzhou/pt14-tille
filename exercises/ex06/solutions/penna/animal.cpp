#include "animal.hpp"
#include <cassert>


namespace Penna
{

// Definition of static data members
age_type Animal::bad_threshold_ = 2;
age_type Animal::maturity_age_ = 0;
double Animal::probability_to_get_pregnant_ = 1.;

void Animal::set_bad_threshold( age_type t )
{
    bad_threshold_ = t;
}

void Animal::set_maturity_age( age_type r )
{
    maturity_age_ = r;
}

void Animal::set_probability_to_get_pregnant( double p)
{
    probability_to_get_pregnant_ = p;
}

/// Default constructor: Uses all good genome.
Animal::Animal()
:   gen_()
,   age_(0)
,   pregnant_(false)
{
}

/// Constructor using a given genome.
Animal::Animal( const Genome& gen )
:   gen_(gen)
,   age_(0)
,   pregnant_(false)
{
}

bool Animal::is_dead() const
{
    return age_ > maximum_age || gen_.count_bad(age_) > bad_threshold_;
}

bool Animal::is_pregnant() const
{
    return pregnant_;
}

age_type Animal::age() const
{
    return age_;
}
	
void Animal::grow()
{
    assert( !is_dead() );
    ++age_;
    std::uniform_real_distribution<double> distribution_01(0,1);
    pregnant_ = (age_ > maturity_age_ && distribution_01(rng)<probability_to_get_pregnant_);
}

Animal Animal::give_birth() const
{
    assert( pregnant_ == true );
    Genome child_genome=gen_;
    child_genome.mutate();
    return Animal( child_genome );
}

} // end namespace Penna
