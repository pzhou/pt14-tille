# Doxygen configuration file

PROJECT_NAME           = "Programming Techniques for Scientific Simulations I"
PROJECT_NUMBER         = 0.1
PROJECT_BRIEF          = "Letting the world know about our projects in a documented way!"
OUTPUT_DIRECTORY       = ./documentation

INPUT                  = integrand_factory_skeleton
FILE_PATTERNS          = *.cpp *.hpp

SOURCE_BROWSER         = YES
INLINE_SOURCES         = YES
GENERATE_HTML          = YES

CALL_GRAPH             = YES
CALLER_GRAPH           = YES
HAVE_DOT               = YES