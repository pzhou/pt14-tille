#ifndef INTEGRAND_GUARD_
#define INTEGRAND_GUARD_

#include <string>
#include <cmath>
#include "function.hpp"

struct sin_lambda : public Function
{
    sin_lambda( argument_type lambda = 1. ): lambda_(lambda){}
    
    result_type operator()( argument_type x ) const { return std::sin(lambda_*x);  }

private:
    argument_type lambda_;
};

struct cos_lambda : public Function
{
    cos_lambda( argument_type lambda = 1. ): lambda_(lambda){}
    
    result_type operator()( argument_type x ) const { return std::cos(lambda_*x);  }

private:
    argument_type lambda_;
};

struct exp_lambda : public Function
{
    exp_lambda( argument_type lambda = 1. ): lambda_(lambda){}
    
    result_type operator()( argument_type x ) const { return std::exp(lambda_*x); }

private:
    argument_type lambda_;
};

Function* integrandFactory(std::string function_name , Function::argument_type lambda );

#endif /* INTEGRAND_GUARD_ */
