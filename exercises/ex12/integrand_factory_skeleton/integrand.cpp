#include <string>
#include "function.hpp"
#include "integrand.hpp"

Function* integrandFactory(std::string function_name , Function::argument_type lambda )
{
    if (function_name=="sin_lambda") {
        return new sin_lambda( lambda );
    } else if (function_name=="cos_lambda") {
        return new cos_lambda( lambda );
    } else if (function_name=="exp_lambda") {
        return new exp_lambda( lambda );
    } else {
        throw std::range_error("Choose an integrand: 'sin_lambda', 'cos_lambda', 'exp_lambda'.");
    }
}

