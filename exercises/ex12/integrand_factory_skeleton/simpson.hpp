#ifndef SIMPSON_GUARD_
#define SIMPSON_GUARD_

#include "function.hpp"

Function::result_type integrate(const Function::argument_type a, const Function::argument_type b, const unsigned bins, const Function& f);

#endif /* SIMPSON_GUARD_ */
