#include <iostream>
#include <string>

#include "function.hpp"
#include "integrand.hpp"
#include "simpson.hpp"

int main(int argc, char ** argv)
{
    if (argc != 5 && argc != 6 ) {
        std::cerr << argv[0] << " <a> <b> <bins> <function_name> <lambda (optional)> " << std::endl;
        exit(1);
    }
    
    double a = std::stod( argv[1] );
    double b = std::stod( argv[2] );
    unsigned bins = std::stoi( argv[3] );
    
    double lambda = 1.;
    if (argc == 6)
        lambda = std::stod( argv[5] );
    
    Function* myintegrand = integrandFactory( std::string( argv[4] ) , lambda );
    
    std::cout.precision(15);
    std::cout << "Integral is: " << integrate( a , b , bins , *myintegrand ) << std::endl;
    
    delete myintegrand;
    return 0;
}
