cmake_minimum_required(VERSION 2.8)

project(RandomAndTimerLibaries)

# add random number generator library
add_library(random STATIC random.cpp)

# Add executable
add_executable(main main.cpp)
target_link_libraries(main random)

install(TARGETS main DESTINATION bin)
install(TARGETS random
		ARCHIVE DESTINATION lib
		LIBRARY DESTINATION lib
		)
install(FILES random.hpp DESTINATION include)