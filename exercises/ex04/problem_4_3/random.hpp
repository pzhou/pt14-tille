#ifndef GENERATOR_H
#define GENERATOR_H

class Generator {
public:
    Generator();                     // use the current time as the initial seed
    Generator(const int initial_seed);    // with initial seed
    Generator(const int initial_seed, const long m, const int a, const int c);

    long generate();
    long max();

protected:
    long _x_n;
    const int _a;
    const int _c;
    const long _m;
};
#endif