#include <iostream>
#include <stdio.h>
#include <limits>
#include <iomanip>

using namespace std;

/*template <class T>
T get_machine_epsilon (T number) {
	T step = 1;
	T result;

	while (result != 1.0) {
		result = 1.0 + step;
		step = step/2;
	}

	return step*2;
}

int main () {
	printf("Machine epsilon (float): %e \n", get_machine_epsilon(1.0f));
	printf("Machine epsilon (double): %e \n", get_machine_epsilon(1.0));
}*/

template <class T>
T get_machine_epsilon () {
	T eps = 1;

	while (T(1) + eps != 1) {
		eps /= 2;
	}

	return eps*2;
}

int main () {
	std::cout << left;
	std::cout << setw(40) << "Machine epsilon (float): " 		<< get_machine_epsilon<float>() 		<< endl;
	std::cout << setw(40) << "Machine epsilon (double): " 		<< get_machine_epsilon<double>() 		<< endl;
	std::cout << setw(40) << "Machine epsilon (long double): " 	<< get_machine_epsilon<long double>() 	<< endl;

	/*printf("Machine epsilon (float): %e \n", get_machine_epsilon<float>());
	printf("Machine epsilon (double): %e \n", get_machine_epsilon<double>());
	printf("Machine epsilon (long double): %e \n\n", get_machine_epsilon<long double>());

	printf("Results of std::numeric_limits<T>::epsilon() \n");
	printf("Machine epsilon (float): %e \n", std::numeric_limits<float>::epsilon());
	printf("Machine epsilon (double): %e \n", std::numeric_limits<double>::epsilon());
	printf("Machine epsilon (long double): %e \n", std::numeric_limits<long double>::epsilon());
	cout << std::numeric_limits<long double>::epsilon() << endl;*/
}