from numpy import *
from matplotlib.pyplot import *
import sys

figure()
means = loadtxt('mean.dat',usecols=(1,3))
errorbar(range(means.shape[0]),means[:,0],yerr=means[:,1],fmt='x',label="estimate from individual runs")
plot((0,means.shape[0]-1),(0,0),label="exact value")
xlabel('run number')
ylabel('mean')
legend(loc="best")
savefig("mean.pdf")

figure()
stddev = loadtxt('stddev.dat',usecols=(1,3))
errorbar(range(stddev.shape[0]),stddev[:,0],yerr=stddev[:,1],fmt='x',label="estimate from individual runs")
plot((0,stddev.shape[0]-1),(1.3526,1.3526),label="exact value")
xlabel('run number')
ylabel('standard deviation')
legend(loc="best")
savefig("stddev.pdf")

figure()
acc_rate = loadtxt('acc_rate.dat',usecols=(2,4))
errorbar(range(acc_rate.shape[0]),acc_rate[:,0],yerr=acc_rate[:,1],fmt='x',label="estimate from individual runs")
plot((0,acc_rate.shape[0]-1),(0.567668,0.567668),label="exact value")
xlabel('run number')
ylabel('acceptance rate')
legend(loc="best")
savefig("acc_rate.pdf")

show()
