#include <iostream>
int main() {

  union {char c[4]; uint32_t q;} u = {{1,0,0,0}};
  if(u.q==1)
    std::cout << "little endian" << std::endl;
  else
    std::cout << "big endian" << std::endl;

  return 0;
}
