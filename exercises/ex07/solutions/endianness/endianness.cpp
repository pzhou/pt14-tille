/* Programming Techniques for Scientific Simulations, HS 2014
 * Exercise 7.1
 */

#include <iostream>

int main() {
    typedef unsigned long long uint_type;
    const unsigned n_bytes = sizeof(uint_type);
    
    /* we create number i = \sum_{n=0}^{n_bytes-1} (7*n+3)*256^n
     * for an 8-byte unsigned integer it would be
     *   i = 342D261F18110A03 (in hexadecimal) = 52*256^7 + 45*256^6 + 38*256^5 + 31*256^4 + 24*256^3 + 17*256^2 + 10*256 + 3
     * assuming atomic element to be a single byte
     * written in 8 bytes ...    big endian = 34 2D 26 1F 18 11 0A 03
     *                    ... little endian = 03 0A 11 18 1F 26 2D 34
     */
    uint_type order(1), i(0);
    for (unsigned byte=0; byte<n_bytes; ++byte) {
      i += (7*byte+3)*order;
      order *= 256;
    }
    
    unsigned char* pos_ = reinterpret_cast<unsigned char*>(&i);
    // check for Big-Endian
    bool consistent=true;
    for (unsigned byte=0; byte<n_bytes; ++byte)
      if (pos_[byte]!=7*byte+3) { consistent=false; break; }
    if (consistent) {
      std::cout<<"Your machine is Little-Endian."<<std::endl;
      // return 0;
    }
    consistent=true;
    for (unsigned byte=0; byte<n_bytes; ++byte)
      if (pos_[byte]!=7*(n_bytes-byte)+3) { consistent=false; break; }
    if (consistent)
      std::cout<<"Your machine is Big-Endian."<<std::endl;
    else
      std::cout<<"Your machine is unknown endian."<<std::endl;

    return 0;
}
