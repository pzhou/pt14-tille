#include <iostream>
#include <string>
#include <cassert>
#include "iterator.hpp"
#include "array.hpp"


struct iterator_unit_tests
{	
	// Maximum test array size
	static const std::size_t max_size = 5;
	
	/* Tests for correct functionality */
	
	static void DefaultConstructible()
	{
		Iterator<double>();
	}
	
	static void NonDefaultConstructor()
	{
		for( std::size_t size_i = 0 ; size_i < max_size ; ++size_i ){ // Array sizes
			
			Array<double> arr( size_i );
			
			for( std::size_t loc_i = 0 ; loc_i <= size_i ; ++loc_i ){ // Array locations				
				
				Iterator<double> ia( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );
				
				assert( ia.pcurr_ == &arr[0] + loc_i );
				assert( ia.pbegin_ == &arr[0] );
				assert( ia.pend_ == &arr[0] + arr.size() );				
			}			
		}		
	}
	
	static void equality()
	{
		for( std::size_t size_i = 1 ; size_i < max_size ; ++size_i ){
			
			Array<double> arr( size_i );
			
			for( std::size_t loc_i = 0 ; loc_i <= size_i ; ++loc_i ){				
				
				Iterator<double> ia( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );
				Iterator<double> ib( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );				
				
				//Equality
				assert( ia == ib );	
				
				//NotEquality
				assert( !(ia != ib) == (ia == ib) );			
			}			
		}			
	}
	
	static void not_equality()
	{
		for( std::size_t size_i = 1 ; size_i < max_size ; ++size_i ){
			
			Array<double> arr( size_i );
			Iterator<double> ia( &arr[0] , &arr[0] , &arr[0] + arr.size() );
			
			for( std::size_t loc_i = 0 ; loc_i <= size_i ; ++loc_i ){			
				
				Iterator<double> ib( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );				
				
				assert( (ia != ib) == !(ia == ib) );				
			}			
		} 
	}
	
	static void CopyConstructible()
	{		
		for( std::size_t size_i = 0 ; size_i < max_size ; ++size_i ){
			
			Array<double> arr( size_i );
			
			for( std::size_t loc_i = 1 ; loc_i <= size_i ; ++loc_i ){			
				
				Iterator<double> ia( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );
				Iterator<double> ib ( ia );
				
				assert( ia == ib );				
			}			
		}
	}
			
	static void CopyAssignable()
	{
		for( std::size_t size_i = 0 ; size_i < max_size ; ++size_i ){
			
			Array<double> arr( size_i );
			
			for( std::size_t loc_i = 1 ; loc_i <= size_i ; ++loc_i ){				
				
				Iterator<double> ia( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );
				Iterator<double> ib;
				
				ib = ia;
				
				assert( ia == ib );				
			}			
		}		
	}
	
	static void dereference_arrow()
	{
		for( std::size_t size_i = 1 ; size_i < max_size ; ++size_i ){ // Need at least one element to be able to dereference
			
			Array< Array<double> > arr( size_i );
			
			for( std::size_t loc_i = 0 ; loc_i < size_i ; ++loc_i ){		
				
				Iterator< Array<double> > ia( &arr[0] + loc_i , &arr[0] , &arr[0] + arr.size() );
				
				// Dereference
				assert( &(*ia) == &arr[0] + loc_i );
				
				//Arrow
				assert( ia->size() == arr[loc_i].size() );				
			}			
		}		
	}
	
	static void preincrement_postincrement()
	{
		for( std::size_t size_i = 2 ; size_i < max_size ; ++size_i ){
			
			Array<double> arr( size_i );
			Iterator<double> ia( arr.begin() );
			Iterator<double> ib( ia );
			
			for( std::size_t loc_i = 0 ; loc_i < size_i - 1 ; ++loc_i ){			
				
				// Preincrement
				assert( &(*++ia) == &arr[loc_i] + 1 );
				
				//Postincrement
				assert( &(*ib++) == &arr[loc_i] );				
			}
			
			assert( &(*ib++) == &arr[0] + (size_i - 1 ) );			
		} 
	}	
	
	static void predecrement_postdecrement()
	{
		for( std::size_t size_i = 1 ; size_i < max_size ; ++size_i ){
			
			Array<double> arr( size_i );
			Iterator<double> ia( arr.end() );
			Iterator<double> ib( ia );
			
			ib--;
			
			for( std::size_t loc_i = 1 ; loc_i < size_i ; ++loc_i ){			
				
				//Predecrement				
				assert( &(*--ia) == &arr[ arr.size() - loc_i ] );
				
				//Postdecrement
				assert( &(*ib--) == &arr[ arr.size() - loc_i ] );								
			}			
		}
	}
	
	static void test_program()
	{
		/* Print message in normal order */
		std::string hello = "Hello ETH students!";

		std::cout << "Original message:" << std::endl;
		std::cout << hello << std::endl;

		Array<std::string> array( hello.size() );
	
		/* Load message into array and print in reverse order */

		Array<std::string>::iterator iter = array.end();

		// Walk to the beginning
		while( iter != array.begin() )
	                iter--;	

		if(iter == array.begin())
			std::cout << "Ready to load... loading!" << std::endl;

		// Load the message
		for( unsigned i = 0 ; iter != array.end() ; ++i )
			iter++->push_back(hello[ i ]);

		if(iter == array.end())
			std::cout << "Finished loading. Printing message in reverse order... " << std::endl; 

		// Print message in reverse order
		while( iter != array.begin() )
			std::cout << *--iter;
		std::cout << std::endl;

		// Go back to where we started
		if(iter == array.begin())
			std::cout << "Going back to starting position..." << std::endl;

		while( iter != array.end() )
	                ++iter;

		if(iter == array.end())
	                std::cout << "Done!" << std::endl;	
	}
	
	static void correct_behavior()
	{		
		DefaultConstructible();
		CopyConstructible();
		CopyAssignable();
		equality();
		not_equality();
		dereference_arrow();
		preincrement_postincrement();
		predecrement_postdecrement();
		
		std::cout << "All basic tests succeeded. Running small test program..." << std::endl;
		
		// And a small test program to get the operators in action
		
		test_program();		
		
		std::cout << "All tests succeeded!" << std::endl;
	}
	
	/* Tests for failure */
	
	static void dereference_past_right_edge()
	{
		Array<double> t(1);
		Iterator<double> i(t.end());
	
		*i;	
	}
	
	static void create_invalid_before_begin()
	{
		Array<double> t(1);
		Iterator<double> i( &t[0] - 1, &t[0] , &t[0] + t.size() );
	}	

	static void create_invalid_after_end()
	{
		Array<double> t(1);
		Iterator<double> i( &t[0] + t.size() + 1 , &t[0] , &t[0] + t.size() );
	}

	static void arrow_past_right_edge()
	{
		Array<std::string> t(1);
		Iterator<std::string> i(t.end());
	
		i->size();	
	}

	static void preincrement_past_right_edge()
	{
		Array<double> t(1);
		Iterator<double> i( t.end() );
	
		++i;	
	}

	static void postincrement_past_right_edge()
	{
		Array<double> t(1);
		Iterator<double> i( t.end()  );
	
		i++;	
	}

	static void predecrement_at_begin()
	{
		Array<double> t(1);
		Iterator<double> i( t.begin() );
	
		--i;	
	}


	static void postdecrement_at_begin()
	{
		Array<double> t(1);
		Iterator<double> i( t.begin()  );
	
		i--;	
	}

};

int main()
{
	
	
	/* Test for correct functionality */
		
	iterator_unit_tests::correct_behavior();	
	
	/* Tests for failure */
	
	//iterator_unit_tests::dereference_past_right_edge();
	//iterator_unit_tests::create_invalid_before_begin();
	//iterator_unit_tests::create_invalid_after_end();
	//iterator_unit_tests::arrow_past_right_edge();
	//iterator_unit_tests::preincrement_past_right_edge();
	//iterator_unit_tests::postincrement_past_right_edge();
	//iterator_unit_tests::predecrement_at_begin();
	//iterator_unit_tests::postdecrement_at_begin();


	return 0;
}
