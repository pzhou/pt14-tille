#include <iostream>
#include <bitset>
#include <limits> // numeric_limits
#include <stdint.h> // typedef for uint_8
#include <algorithm> // std::distance, std::find
#include <assert.h>

using namespace std;

// note: ~0 is a 64bit int with ones only so use ~0u when in 32bit context
// note: bitshift operators abstract away endianness

// just a helper function (can be replaced by cout << std::bitset<32>(a) << endl;)
// prints the binary represantation in big-endianness
void printbinary (int a) {
    for (int i=32; i>0; i--) {
        cout << (bool) (a & (1 << (i-1)));
    }
    cout << std::endl;
}

enum Endianness {
    LITTLE, BIG, MIXED
};

template <bool is_specialized, bool is_integer, bool is_signed> struct argument_type {
    static const bool valid = false;
};

// isSpecialized && isInteger && !isSigned
template <> struct argument_type<true, true, false> {
    static const bool valid = true;
};

// Determine endianness of your machine for given type
// Pre: smallest memory unit is 1 byte,
//        T is an unsigned integral type
template <class T>
Endianness determineEndianness() {
    // verify that argument is unsigned and integral
    //    be aware that since assert is a macro double paranthesis are required
    assert((argument_type<numeric_limits<T>::is_specialized,
                            numeric_limits<T>::is_integer,
                            numeric_limits<T>::is_signed>::valid));

    //have you ever seen a machine with different endianness for different types?:)
    //32-bit unsigned int (uint32_t) should do

    const int digits = numeric_limits<T>::digits;
    const int num_words = numeric_limits<T>::digits/8;

    T a(0);
    uint8_t* words = (uint8_t*) &a;
    int changedWords[num_words] = {0};

    for (int i=0; i < num_words; i++) {
        a = numeric_limits<T>::min()+255 << i*8; // set i-th byte (from a big-endianness view) to all ones

        changedWords[i] = std::distance(words, std::find(words, words + num_words, 255)); // determine which word has changed
    }

    if (is_sorted(changedWords, changedWords+num_words)) // asc
        return LITTLE;
    else if (is_sorted(changedWords, changedWords+num_words, std::greater<T>())) // desc
        return BIG;

    return MIXED;

    /*

      check out "union". we can do this in 2 lines (if we dont check for mixed)

      union {char c[4]; uint32_t q;} u = {{1,0,0,0}};
      return (u.q==1 ? LITTLE : BIG);

     */
};

// determine endianess of your machine for int
int main () {
    switch (determineEndianness<unsigned int>()) {
        case LITTLE:
            cout << "little-endianness";
            break;
        case BIG:
            cout << "big-endianness";
            break;
        case MIXED:
            cout << "mixed-endianness";
            break;
        default: throw; break;
          //break is not needed, as "default:" is always the last statement in a switch
          //if you throw an exception, you should also catch it, usually printing an message and aborting
    }
}
