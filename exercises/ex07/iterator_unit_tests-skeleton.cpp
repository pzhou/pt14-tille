#include "iterator.hpp"
#include "array.hpp"

struct iterator_unit_tests
{
	/* Tests for success */
	
	static void dereference()
	{
		// ...
	}
	
	static void test_program()
	{
		// ...		
	}
	
	/* Tests for failure */
	
	static void dereference_past_right_edge()
	{
		Array<double> t(1);
		Iterator<double> i(t.end());
	
		*i;	
	}
	
	static void create_invalid_before_begin()
	{
		// ...
	}	

	static void create_invalid_after_end()
	{
		// ...
	}

	// ...

};


int main()
{
	
	// ...
	iterator_unit_tests::dereference_past_right_edge();
	
	// ...

	return 0;
}
