# Simpson integration
macro simpson_integration_step(f, x, Δx)
    return :(f($x) + 4*f($x+$Δx/2) + f($x+$Δx))
end

function simpson_integration(f, a, b; N=1, parallel=true)
    Δx::BigFloat = (b-a)/N
    result::BigFloat = 0

    if (parallel)
        # map simpson integration step to processes
        discretization = pmap((i) -> @simpson_integration_step(f, i*Δx, Δx), [0:N-1])

        # sort results for higher precision
        sort(discretization)
        result = BigFloat(0)
        for i = [1:N]
            result += discretization[i]
        end

        return (Δx/6)*result
    else
        for i=0:N-1
            result += @simpson_integration_step(f, i*Δx, Δx)
        end

        return (Δx/6)*result
    end
end
