#include <iostream>
#include "include/simpson_integration.h"
#include <math.h>
//#include <cmath> - c++ style

#include "tclap/CmdLine.h" // command line argument parser

double f(double x) {
  //double f(const double x) {
	return sin(x);
}

int main (int argc, char** argv) {
	try{
		// parse cmd line args
		// TODO: use cmake version number
		TCLAP::CmdLine cmd("Computes an approximation for the integral of sin(x) from a to b with N bits");

		TCLAP::ValueArg<double> a("a", "lower-boundary", "lower Boundary", false, 0., "double");
		TCLAP::ValueArg<double> b("b", "upper-boundary", "upper Boundary", false, 3.1415926535897, "double");
		TCLAP::ValueArg<double> N("N", "bins", "number of bins", false, 100, "int");
		TCLAP::ValueArg<double> error_to("e", "error-to", "print absolute and relative error", false, 2., "int");
		
		cmd.add(a);
		cmd.add(b);
		cmd.add(N);
		cmd.parse( argc, argv );

		printf("\\int_{%g}^{%g} sin(x) dx with %g bins \n", a.getValue(), b.getValue(), N.getValue());

		// Calculations
		double result = simpson_integration(&f, a.getValue(), b.getValue(), N.getValue());
                //const double result = ... - use const whenever possible

                //use std::cout instead of printf

		// Print results
		printf("%-20s%-20s%-20s\n", "abs_err", "rel_err", "result");
		printf("%-20e%-20e%-20g\n", (result-error_to.getValue())/result, (result-error_to.getValue()), result);

		return 0;
	} catch (TCLAP::ArgException &e)  // catch any exceptions
	{ std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }	
}
