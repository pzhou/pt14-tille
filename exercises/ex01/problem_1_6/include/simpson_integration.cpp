#include "simpson_integration.hpp"

/**
 * Simpson integration step
 *  see http://de.wikipedia.org/wiki/Simpsonregel
 */
template <typename F>
inline double simpson_integration_step (F f, double x, double delta_x) {
	return (f(x) + 4*f(x+delta_x/2) + f(x+delta_x));
}

/**
 * Simpson integration
 * @var f pointer to the function to integrate
 * @var a lower bound
 * @var b upper bound
 * @var N number of steps 
 */
template <typename F>
double simpson_integration (F f, const double a, const double b, const int N) {
  //double simpson_integration (double (*f)(double), const double a, const double b, const int N)
  double x; // - where is this used
	double delta_x = (b-a)/N;
        //const double delta_x = (b-a)/N;
	double result;
        //double result(0.0); - always initialize with something

	for (int i=0; i < N; i++) {
		result += simpson_integration_step(f, i*delta_x, delta_x);
	}

	return (delta_x/6)*result;
}

// explicit declaration
template double simpson_integration<double (*)(double)>(double (*f)(double), double a, double b, int N);
template double simpson_integration<MathFunction>(MathFunction f, double a, double b, int N);