#include <iostream>
#include <limits> // for machine epsilon
#include "simpson_integration.h"

using namespace std;

double f(double x) {
	return x*(1-x);
}

/*int main () {
	for (int i=0; i<=10; i++) {
		assert(simpson_integration(&f, 0, 1, i)-1f/6f < numeric_limits<float>::epsilon());
	}
}*/

int main () {
	printf("Result: %e", simpson_integration(&f, 0, 1, 10));
}

