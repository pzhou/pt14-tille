# Simpson Integration

Implementation of the simpson integration for usage as a library for c++ programs or as a command line utlity. This C++ library is mainly for demonstration of the cmake build tool, but there is an expanded commandline utility written in julia for numerical approches. 

`Performance`

When you build the library with a C++ function there should be very little overheat. Still the initialization of julia is quite expensive so don't expact to much from the expanded command line utility unless you are have a high number of iterations

## Usage

### as a library

see `include/simpson_integration.h`

	/**
	 * Simpson integration
	 * @var f pointer to the function to integrate
	 * @var a lower bound
	 * @var b upper bound
	 * @var N number of steps 
	 */
	double simpson_integration (double (*f)(double), double a, double b, int N);

### c++ cmd

The C++ library has a simple commandline interface which is quite useless for numerical approches as of low precision

```
$ ./simpson_integration --help
USAGE: 

./simpson_integration  [-N <int>] [-b <double>] [-a <double>] [--]
                          [--version] [-h]

Where: 

   -N <int>,  --bins <int>
     number of bins

   -b <double>,  --upper-boundary <double>
     upper Boundary

   -a <double>,  --lower-boundary <double>
     lower Boundary

   --,  --ignore_rest
     Ignores the rest of the labeled arguments following this flag.

   --version
     Displays version information and exits.

   -h,  --help
     Displays usage information and exits.


   Computes an approximation for the integral of sin(x) from a to b with N
   bits

$ ./simpson_integration
\int_{0}^{3.14159} sin(x) dx with 100 bins 
abs_err             rel_err             result              
3.382357e-10        6.764713e-10        2 

$ ./simpson_integration -a 0 -b 3.1415926535897 -N 10
\int_{0}^{3.14159} sin(x) dx with 10 bins 
abs_err             rel_err             result              
3.392209e-06        6.784442e-06        2.00001
	
```

### julia cmd (expanded cmd utility)

The julia implementation is a bit more suitable for numerical approches.

Here is an example for the approximation of \int_0^1 x(1-x) dx 

	$ julia simpson_integration.jl "f(x) = x*(x-1)" 0 1 10

Or the approximation of \int_0^\pi sin() dx from the excercise with multiple precisions and bins

```
$  julia simpson_integration.jl --precision="[2^i for i=8:10]" --error-to="2" "sin(x)" 0 "big(pi)"  "[2^i for i=0:8]"                              [17:58:48]
Simpson approximation of int_a^b f(x) dx with 
   f: sin(x) 
   a: 0 
   b: big(pi) 
   N: [1,2,4,8,16,32,64,128,256] 
   total iterations: 1.533000e+03 
precision N         abs_err   rel_err   
256       1         9.44e-02  4.72e-02  2.094395e+00        
256       2         4.56e-03  2.28e-03  2.004560e+00        
256       4         2.69e-04  1.35e-04  2.000269e+00        
256       8         1.66e-05  8.30e-06  2.000017e+00        
256       16        1.03e-06  5.17e-07  2.000001e+00        
256       32        6.45e-08  3.23e-08  2.000000e+00        
256       64        4.03e-09  2.02e-09  2.000000e+00        
256       128       2.52e-10  1.26e-10  2.000000e+00        
256       256       1.57e-11  7.87e-12  2.000000e+00        
512       1         9.44e-02  4.72e-02  2.094395e+00        
512       2         4.56e-03  2.28e-03  2.004560e+00        
512       4         2.69e-04  1.35e-04  2.000269e+00        
512       8         1.66e-05  8.30e-06  2.000017e+00        
512       16        1.03e-06  5.17e-07  2.000001e+00        
512       32        6.45e-08  3.23e-08  2.000000e+00        
512       64        4.03e-09  2.02e-09  2.000000e+00        
512       128       2.52e-10  1.26e-10  2.000000e+00        
512       256       1.57e-11  7.87e-12  2.000000e+00        
1024      1         9.44e-02  4.72e-02  2.094395e+00        
1024      2         4.56e-03  2.28e-03  2.004560e+00        
1024      4         2.69e-04  1.35e-04  2.000269e+00        
1024      8         1.66e-05  8.30e-06  2.000017e+00        
1024      16        1.03e-06  5.17e-07  2.000001e+00        
1024      32        6.45e-08  3.23e-08  2.000000e+00        
1024      64        4.03e-09  2.02e-09  2.000000e+00        
1024      128       2.52e-10  1.26e-10  2.000000e+00        
1024      256       1.57e-11  7.87e-12  2.000000e+00        
elapsed time: 0.982365815 seconds (24858229 bytes allocated)
```

See the help for more information. Note that currently the parallel mode is quite slow even though it uses asynchronious processing and multithreading if julia is started likewise

```
usage: simpson_integration.jl [--verbose VERBOSE]
                        [--precision PRECISION] [--error-to ERROR-TO]
                        [--parallel PARALLEL] [-h] function a b [N]

positional arguments:
  function              the function to integrate
  a                     lower boundary
  b                     upper boundary
  N                     steps

optional arguments:
  --verbose VERBOSE     verbose information (default: false)
  --precision PRECISION
                        define floating point precision (default: 64)
  --error-to ERROR-TO   print relative error (default: false)
  --parallel PARALLEL   run approximation in paralell (type: Bool,
                        default: false)
  -h, --help            show this help message and exit
```

## Tests
The test simply verifies that \int_0^1 x(1-x) dx is 1/6, the julia library has no tests currently

`simpson_integration_test.cpp`

```
double f(double x) {
	return x*(1-x);
}

int main () {
	double result = simpson_integration(&f, 0, 1, 1);

	// Result should be 1/6
	assert((result - double(1)/6) < std::numeric_limits<double>::epsilon());

	return 0;
}

```

Run tests

	$ make test
	

```
Running tests...
Test project <path>/exercises/ex01/problem_1_6
    Start 1: polynomial_of_degree_two
1/1 Test #1: polynomial_of_degree_two .........   Passed    0.01 sec

100% tests passed, 0 tests failed out of 1

Total Test time (real) =   0.01 sec
```


## Build

To build the C++ binary of this project. Note that the julia implementation can be called directly from the repository.

	make
	make test

### Build options

`--with-inline-machine-epsilon`

Calculate machine epsilon for float and double manually

Note: not implemented yet, this would be nice to see with cmake since we calculated it in ex1.5

## TODO

Currently the library just uses C++ double precision operations for the calculation but it should be easy to use julias interface for C++ for floating point operations with arbitrary precision. For the sake of completeness it would be nice to habe the GMP lib included directly.