require("argparse")
using ArgParse

function parse_commandline(args)
    s = ArgParseSettings()

    @add_arg_table s begin
        "--verbose"
            help = "verbose information"
            default = false
        "--precision"
            help = "define floating point precision"
            default = 64
        "--error-to"
            help = "print relative error"
            default = false
        "--parallel"
            help = "run approximation in paralell"
            default = false
            arg_type = Bool
        "function"
            help = "the function to integrate"
            required = true
        "a"
            help = "lower boundary"
            required = true
            #arg_type = BigFloat
        "b"
            help = "upper boundary"
            required = true
            #arg_type = BigFloat
        "N"
            help = "steps"
    end

    return parse_args(args, s)
end

require("simpson_integration_library.jl")

function main(args)
    args = parse_commandline(args)

    # verbose
    if (args["verbose"])
        println(args)
        println("Parsed args:")
        for pa in args
            println("  $(pa[1])  =>  $(pa[2])")
        end
    end
    # end

    # args
    f = eval(parse(string("x -> ", args["function"])))
    a = args["a"] # lower boundary
    b = args["b"] # upper boundary
    N = eval(parse(args["N"])) # steps
    @assert (typeof(N) == Int || typeof(N) == Array{Int, 1}) "N must be of type Int or Array{Int, 1}"
    typeof(N) != Array{Int, 1} ? N = [N] : nothing

    # options
    p = eval(parse(args["precision"])) # precision
    @assert (typeof(p) == Int || typeof(p) == Array{Int, 1}) "precision must be of type Int or Array{Int, 1}"
    typeof(p) != Array{Int, 1} ? p = [p] : nothing

    error_to = args["error-to"]
    if error_to != false
        error_to = eval(parse(args["error-to"]))
    end

    parallel = args["parallel"]

    @printf("Simpson approximation of \int_a^b f(x) dx with \n")
    @printf("   f: %s \n", args["function"])
    @printf("   a: %s \n", a)
    @printf("   b: %s \n", b)
    @printf("   N: %s \n", N)
    @printf("   total iterations: %e \n", length(p)*sum(N))

    # Integrate
    @printf("%-10s%-10s%-10s%-10s\n", "precision", "N", "abs_err", "rel_err")
    @time for precision in p, n in N
        with_bigfloat_precision(precision) do
            # parse boundaries with higher precision
            a = eval(parse(args["a"]))
            b = eval(parse(args["b"]))

            result = simpson_integration(f, a, b, N=n, parallel=parallel)

            # Print absolut error
            if (args["error-to"] != false)
                if (args["verbose"])
                    @printf("Next float: %e", prevfloat(oftype(BigFloat, result)))
                    @printf("Prev float: %e", nextfloat(oftype(BigFloat, result)))
                end
            end

            # Absolute / relative error calculation should have at least 64bit precision
            abs_err = 0
            if (precision < 64)
                abs_err = abs(oftype(Float64, result)-oftype(Float64, error_to))
                rel_err = abs_err/oftype(Float64, error_to)
            else
                abs_err = abs(result-error_to)
                rel_err = abs((result-error_to)/error_to)
            end

            @printf("%-10d%-10d%-10.2e%-10.2e%-20e\n", precision, n, abs_err, rel_err, result)
        end
    end
end

main(ARGS)
