#include <iostream>
#include <stdio.h>

using namespace std;

template <class T>
T get_machine_epsilon (T number) {
	T step = 1;
	T result;

	while (result != 1.0) {
		result = 1.0 + step;
		step = step/2;
	}

	return step*2;
}

//use std::cout (c++ style, especially as you included iostream)
//instead of printf (C-style)

int main () {
	printf("Machine epsilon (float): %e \n", get_machine_epsilon(1.0f));
	printf("Machine epsilon (double): %e \n", get_machine_epsilon(1.0));
}
