# build

	cmake .
	make

# Usage

	$ ./machine_epsilon
	Machine epsilon (float): 2.980232e-08 
	Machine epsilon (double): 5.551115e-17
