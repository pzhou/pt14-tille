set term png; 
set output 'simpson_benchmark.png';
set grid; 
set xlabel 'fn'; 
set ylabel 'time[s]'; 
set title 'Simpson integration benchmark'; 
plot \
"simpson_benchmark.dat" using 1:2 w lp title "hard coded", \
"simpson_benchmark.dat" using 1:3 w lp title "function pointer", \
"simpson_benchmark.dat" using 1:4 w lp title "virtual function", \
"simpson_benchmark.dat" using 1:5 w lp title "templated functor";