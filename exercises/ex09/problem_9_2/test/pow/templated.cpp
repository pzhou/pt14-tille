template <class T, class V, unsigned int n>
struct pow {
	typedef T result_type;
    typedef V argument_type;

	T operator() (const V x) const {
		/* the compiler will inline this! */
		return x * pow<T, V, n-1>()(x);
	}
};

template <class T, class V>
struct pow<T, V, 0> {
	typedef T result_type;
    typedef V argument_type;

	T operator() (const V x) const { return 1; }
};

double cubic (const double x) {
	pow<double, double, 3> func;
	return func(x);
}