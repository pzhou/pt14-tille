#include <math.h>
#include "math_functions.hpp"

// note that this can stop the compiler from optimizing code
#ifdef NDEBUG
#	define ASM_COMMENT(f)
#else
#	define ASM_COMMENT(f) asm("# benchmark " #f "");
#endif

#define SIMPSON_INTEGRATION_HARD_CODED(f)								\
static double simpson_integration_ ## f (const double a, const double b, const unsigned bins) {	\
  ASM_COMMENT(f)														            \
  const double dr = (b - a) / (2.*bins); 								\
  																		                  \
  double I = f(a);														          \
  																		                  \
  for(unsigned int i = 1; i < 2*bins; ++i)							\
    I += 2 * (1.0 + i%2) * f(a + dr * i);								\
  																		                  \
  I += f(b); 															              \
  																		                  \
  return I * (1./3) * dr; 												      \
}

/*static double simpson_integration_ ## f (const double a, const double b, const unsigned bins) { \
  ASM_COMMENT(f)                                        \
  const double dr = (b - a) / (2*bins);                 \
                                                        \
  double I2 = 0;                                        \
  double I4 = 0;                                        \
                                                        \
  for(unsigned i = 0; i < bins; ++i) {                  \
    double pos = a + (2*i+1.)*dr;                       \
    I4 += f(pos);                                       \
    I2 += f(pos);                                       \
  }                                                     \
                                                        \
  return (f(a) + 2.*I2 + 4.*I4 - f(b)) * (dr/3.);       \
}*/

// define simpson_integration_f1, ..., simpson_integration_f6
SIMPSON_INTEGRATION_HARD_CODED(f1);
SIMPSON_INTEGRATION_HARD_CODED(f2);
SIMPSON_INTEGRATION_HARD_CODED(f3);
SIMPSON_INTEGRATION_HARD_CODED(f4);
SIMPSON_INTEGRATION_HARD_CODED(f5);
SIMPSON_INTEGRATION_HARD_CODED(f6);