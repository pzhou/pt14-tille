#include <assert.h>

double simpson_integration_virtual(const double a, const double b, const unsigned bins, const Functors::UnaryFunction& function) 
{
  assert(bins > 0);

  const unsigned int steps = 2*bins + 1;

  const double dr = (b - a) / (steps - 1);

  double I = function(a);
  
  for(unsigned int i = 1; i < steps-1; ++i)
    I += 2 * (1.0 + i%2) * function(a + dr * i);

  I += function(b);
  
  return I * (1./3) * dr;
}