#include <chrono>
#include <assert.h>
#include <iostream>	// std::cout
#include <iomanip>	// std::setw
#include <fstream>	// std::ofstream
#include "pwd.hpp"
// the functions we want to benchmark
#include "math_functions.hpp"
#include "math_functors.hpp"
#include "math_templated_functors.hpp"
// the different implementations of simpson_integration
#include "implementations/hard_coded.cpp"
#include "implementations/function_pointer.cpp"
#include "implementations/templated_functor.hpp"
#include "implementations/virtual_functor.cpp"

#include <string>
// benchmark helper macro
using std::chrono::system_clock;
using std::chrono::duration;
#define BENCHMARK(expr)											\
[] (std::size_t num_ops) {										\
	/* tic */													\
	auto start = system_clock::now();							\
																\
	auto result = expr;											\
	for (int i=1; i< num_ops; i++) {							\
		result += expr;											\
	}															\
	auto volatile no_dce = result;								\
																\
	/* toc */													\
	auto end = system_clock::now();								\
																\
	std::cout << "benchmark " << #expr << " done" << std::endl;	\
	return duration<double>(end-start).count();					\
}

using std::setw; //you can just declare this right before using setw
int main () {
	std::size_t num_ops = 1e6;

	// integration parameters
	//	need to be const to be implicitly captured in benchmark lambda function
	const int a = 0;
	const int b = 1;
	const int bins = 100;

	// benchmark hard coded implementations
	double results[4][6] = {{
		BENCHMARK(simpson_integration_f1(a, b, bins))(num_ops),
		BENCHMARK(simpson_integration_f2(a, b, bins))(num_ops),
		BENCHMARK(simpson_integration_f3(a, b, bins))(num_ops),
		BENCHMARK(simpson_integration_f4(a, b, bins))(num_ops),
		BENCHMARK(simpson_integration_f5(a, b, bins))(num_ops),
		BENCHMARK(simpson_integration_f6(a, b, bins))(num_ops)
	}, {
		BENCHMARK(simpson_integration(a, b, bins, &f1))(num_ops),
		BENCHMARK(simpson_integration(a, b, bins, &f2))(num_ops),
		BENCHMARK(simpson_integration(a, b, bins, &f3))(num_ops),
		BENCHMARK(simpson_integration(a, b, bins, &f4))(num_ops),
		BENCHMARK(simpson_integration(a, b, bins, &f5))(num_ops),
		BENCHMARK(simpson_integration(a, b, bins, &f6))(num_ops)
	}, {
		BENCHMARK(simpson_integration_virtual(a, b, bins, Functors::f1()))(num_ops),
		BENCHMARK(simpson_integration_virtual(a, b, bins, Functors::f2()))(num_ops),
		BENCHMARK(simpson_integration_virtual(a, b, bins, Functors::f3()))(num_ops),
		BENCHMARK(simpson_integration_virtual(a, b, bins, Functors::f4()))(num_ops),
		BENCHMARK(simpson_integration_virtual(a, b, bins, Functors::f5()))(num_ops),
		BENCHMARK(simpson_integration_virtual(a, b, bins, Functors::f6()))(num_ops)
	}, {
		BENCHMARK(simpson_integration_templated(a, b, bins, TemplatedFunctors::mathconst<double, double>(0)))(num_ops),
		BENCHMARK(simpson_integration_templated(a, b, bins, TemplatedFunctors::mathconst<double, double>(1)))(num_ops),
		BENCHMARK(simpson_integration_templated(a, b, bins, TemplatedFunctors::pow<double, double, 1>()))(num_ops),
		BENCHMARK(simpson_integration_templated(a, b, bins, TemplatedFunctors::pow<double, double, 2>	()))(num_ops),
		BENCHMARK(simpson_integration_templated(a, b, bins, TemplatedFunctors::sin_lambda_x<double, double>(1.)))(num_ops),
		BENCHMARK(simpson_integration_templated(a, b, bins, TemplatedFunctors::sin_lambda_x<double, double>(5.)))(num_ops)
	}};

        //usually we use #ifndef NDEBUG to indicate debug regions

#if NDEBUG
	std::string filePath = pwd() + "/simpson_benchmark.dat";
	std::ofstream dataStream ( filePath );
#else
	std::string filePath = "<stdout>";
	std::ostream& dataStream = std::cout;
#endif

        //using std::setw;

	//#f	hard-coded	function-pointer	virtual		templated
	dataStream << "#" << setw(1) << "f"
			   << setw(18) << "hard-coded"
			   << setw(18) << "function-pointer"
			   << setw(18) << "virtual"
			   << setw(18) << "templated" << std::endl;

        //for(unsigned i = 0; i < 6u; ++i)
	for (std::size_t i=0; i<6; i++) {
		dataStream << setw(2) << i;
		for (std::size_t j=0; j<4; j++) {
			dataStream << setw(18) << results[j][i];
		}
		dataStream << '\n';
	}

#if NDEBUG
	try {
		dataStream.close();
		std::cout << "[succ] benchmark results saved to " << filePath << std::endl;
	} catch (...) {
		std::cout << "[err] IO error\n" << std::endl;
	}
#endif

	// show offset


	return 0;
}
