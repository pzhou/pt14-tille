# Simpson integration benchmark

## Usage

	mkdir build
	cd build
	cmake ..

You should now have a plot named `simpson_benchmark.png` in your build directory.

## Testing

Currently the package contains a simple test that checks if the templated `pow` method used for implementation of the *simpson integration with templated functors* actuelly has the same computational complexity than the hardcoded version by comparing the assembler output. To run the tests you have to compile with `BUILD_TYPE=Test` and run `make check`.

## Results

![Sample Result Intel Sandy Bridge](sample/simpson_benchmark_sandy_bridge.png)

As we can see the runtime for the

* hard coded
* templated functor
* function pointer

implementations are mostly equal. If we compare the assembler code of the *hard coded* and the *templated functor* implementations we see that they are actuelly the same. Nevertheless the function pointer had slightly different output. But since the runtime is equal there should be no memory access which implies that the address to the function is fetched only once and stays in the cache.

The gap between the virtual function implementation and the other ones implies that there is memory access in the virtual function implementation to determine the function to call. Since the gap gets smaller with higher complexity (`f5, f6`) there should be some branch preditiction that makes is possible to already compute the function value while determining the actuall function to call.

## File structure

`Test` 	- contains source files for testing purposes
`Debug` - contains source files for debug purposes

