#ifndef MATH_TEMPLATED_FUNCTORS_H_
#	define MATH_TEMPLATED_FUNCTORS_H_

//#include <cmath>
#include <math.h>

// unary function
namespace TemplatedFunctors {
	template <class T, class V>
	struct mathconst {
	    typedef T result_type;
	    typedef V argument_type;

	    const int c;

	    mathconst(const int c) : c(c) {} 

		double operator() (const double x) const { return c; }
	};

	template <class T, class V, unsigned int n>
	struct pow {
		typedef T result_type;
	    typedef V argument_type;

		T operator() (const V x) const {
			/* the compiler will inline this hurray */
			return x* pow<T, V, n-1>()(x);
		}
	};

	template <class T, class V>
	struct pow<T, V, 0> {
		typedef T result_type;
	    typedef V argument_type;

		T operator() (const V x) const { return 1; }
	};

	template <class T, class V>
	struct sin_lambda_x {
	    typedef T result_type;
	    typedef V argument_type;
	    
		sin_lambda_x(const double lambda_value=1.0) : lambda(lambda_value) {}
	    double operator()(const double x) const { return sin(lambda*x); }

	    const double lambda;
	};
}
#endif