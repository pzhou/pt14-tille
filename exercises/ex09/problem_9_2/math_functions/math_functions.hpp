#ifndef MATH_FUNCTIONS_H_
#	define MATH_FUNCTIONS_H_

//#include <cmath>
#include <math.h>

//include <cmath>, std::sin(...), (const double x)

inline double f1(double x) { return 0; }
inline double f2(double x) { return 1; }
inline double f3(double x) { return x; }
inline double f4(double x) { return x*x; }
inline double f5(double x) { return sin(x); }
inline double f6(double x) { return sin(5*x); }
#endif
