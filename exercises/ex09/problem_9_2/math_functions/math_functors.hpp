#ifndef MATH_FUNCTORS_H_
#	define MATH_FUNCTORS_H_

//#include <cmath>
#include <math.h>

namespace Functors {
	// unary function
	struct UnaryFunction {
		virtual double operator() (const double x) const = 0;
	};

	struct f1 : UnaryFunction {
		double operator() (const double x) const { return 0; }
	};

	struct f2 : UnaryFunction {
		double operator() (const double x) const { return 1; }
	};

	struct f3 : UnaryFunction {
		double operator() (const double x) const { return x; }
	};

	struct f4 : UnaryFunction {
		double operator() (const double x) const { return x*x; }
	};

	struct f5 : UnaryFunction {
		double operator() (const double x) const { return sin(x); }
	};

	struct f6 : UnaryFunction {
		double operator() (const double x) const { return sin(5*x); }
	};

};
#endif