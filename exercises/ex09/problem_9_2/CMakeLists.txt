cmake_minimum_required(VERSION 2.8)

project(SimpsonIntegrationBenchmark)

# determine build type
if (NOT CMAKE_BUILD_TYPE)
		message(STATUS "No build type selected, default to Release")
		set(CMAKE_BUILD_TYPE "Release")
endif()

# use c++ 11
SET(CMAKE_CXX_FLAGS "-std=c++0x -O1")
#-funroll-loops

# include dependencies
include_directories( math_functions )
add_executable( main main.cpp )

# run benchmark
add_custom_command(TARGET main
                     POST_BUILD
                     COMMAND main)

# Generate plot
if (CMAKE_BUILD_TYPE STREQUAL "Release")
	FIND_PACKAGE(GNUPLOT)
	add_custom_command(TARGET main
	                     POST_BUILD
	                     COMMAND ${CMAKE_COMMAND} ARGS -E cmake_echo_color --cyan "Successfully generated simpson_benchmark.png")
	add_custom_command(TARGET main
	                     POST_BUILD
	                     COMMAND ${GNUPLOT_EXECUTABLE} ARGS ${CMAKE_SOURCE_DIR}/plot.p)
endif (CMAKE_BUILD_TYPE STREQUAL "Release")

#
# Testing
#
if (CMAKE_BUILD_TYPE STREQUAL "Test")
	enable_testing()

	# make sure that generic pow function actuelly has same complexity as if it was hard coded
	include_directories( test/pow )

	add_library(pow_static test/pow/static.cpp)
	add_library(pow_templated test/pow/templated.cpp)

	set_target_properties(pow_static PROPERTIES COMPILE_FLAGS "-save-temps -O3")
	set_target_properties(pow_templated PROPERTIES COMPILE_FLAGS "-save-temps -O3")

	set_directory_properties(PROPERTIES
		ADDITIONAL_MAKE_CLEAN_FILES "static.s;static.ii;templated.s;templated.ii")

	add_test(NAME generic_pow COMMAND diff static.s templated.s)

	add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND}
	                  DEPENDS pow_templated pow_static)
endif (CMAKE_BUILD_TYPE STREQUAL "Test")