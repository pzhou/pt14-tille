	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI0_0:
	.quad	4607182418800017408     ## double 1
LCPI0_1:
	.quad	4599676419421066581     ## double 0.33333333333333331
	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z19simpson_integrationddjPFddE
	.align	4, 0x90
__Z19simpson_integrationddjPFddE:       ## @_Z19simpson_integrationddjPFddE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp6:
	.cfi_offset %rbx, -40
Ltmp7:
	.cfi_offset %r14, -32
Ltmp8:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movl	%edi, %r15d
	testl	%r15d, %r15d
	je	LBB0_1
## BB#3:
	testq	%r14, %r14
	je	LBB0_4
## BB#5:
	addl	%r15d, %r15d
	movaps	%xmm1, %xmm2
	movsd	%xmm1, -72(%rbp)        ## 8-byte Spill
	subsd	%xmm0, %xmm2
	cvtsi2sdq	%r15, %xmm1
	divsd	%xmm1, %xmm2
	movsd	%xmm2, -56(%rbp)        ## 8-byte Spill
	movsd	%xmm0, -64(%rbp)        ## 8-byte Spill
	callq	*%r14
	cmpl	$2, %r15d
	jb	LBB0_6
## BB#7:                                ## %.lr.ph
	movl	$1, %ebx
	movsd	LCPI0_0(%rip), %xmm1
	movaps	%xmm1, %xmm2
	movsd	%xmm0, -32(%rbp)        ## 8-byte Spill
	.align	4, 0x90
LBB0_8:                                 ## =>This Inner Loop Header: Depth=1
	movsd	%xmm2, -40(%rbp)        ## 8-byte Spill
	movl	%ebx, %eax
	andl	$1, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -48(%rbp)        ## 8-byte Spill
	movsd	-56(%rbp), %xmm0        ## 8-byte Reload
	mulsd	%xmm2, %xmm0
	addsd	-64(%rbp), %xmm0        ## 8-byte Folded Reload
	callq	*%r14
	movsd	-40(%rbp), %xmm2        ## 8-byte Reload
	mulsd	-48(%rbp), %xmm0        ## 8-byte Folded Reload
	movsd	-32(%rbp), %xmm1        ## 8-byte Reload
	addsd	%xmm0, %xmm1
	movsd	%xmm1, -32(%rbp)        ## 8-byte Spill
	movsd	LCPI0_0(%rip), %xmm0
	addsd	%xmm0, %xmm2
	movaps	%xmm0, %xmm1
	incl	%ebx
	cmpl	%ebx, %r15d
	jne	LBB0_8
	jmp	LBB0_9
LBB0_6:
	movsd	%xmm0, -32(%rbp)        ## 8-byte Spill
LBB0_9:                                 ## %._crit_edge
	movsd	-72(%rbp), %xmm0        ## 8-byte Reload
	callq	*%r14
	addsd	-32(%rbp), %xmm0        ## 8-byte Folded Reload
	mulsd	LCPI0_1(%rip), %xmm0
	movsd	-56(%rbp), %xmm1        ## 8-byte Reload
	mulsd	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_1:
	leaq	L___func__._Z19simpson_integrationddjPFddE(%rip), %rax
	leaq	L_.str(%rip), %rcx
	leaq	L_.str1(%rip), %rbx
	movl	$6, %edx
	jmp	LBB0_2
LBB0_4:
	leaq	L___func__._Z19simpson_integrationddjPFddE(%rip), %rax
	leaq	L_.str(%rip), %rcx
	leaq	L_.str2(%rip), %rbx
	movl	$7, %edx
LBB0_2:
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rbx, %rcx
	callq	___assert_rtn
	.cfi_endproc

	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI1_0:
	.quad	4607182418800017408     ## double 1
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp11:
	.cfi_def_cfa_offset 16
Ltmp12:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp13:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__Z2f1d@GOTPCREL(%rip), %rsi
	movsd	LCPI1_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	movl	$1, %edi
	callq	__Z19simpson_integrationddjPFddE
	movsd	%xmm0, -8(%rbp)
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z2f1d
	.weak_definition	__Z2f1d
	.align	4, 0x90
__Z2f1d:                                ## @_Z2f1d
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp16:
	.cfi_def_cfa_offset 16
Ltmp17:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp18:
	.cfi_def_cfa_register %rbp
	xorps	%xmm0, %xmm0
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L___func__._Z19simpson_integrationddjPFddE: ## @__func__._Z19simpson_integrationddjPFddE
	.asciz	"simpson_integration"

L_.str:                                 ## @.str
	.asciz	"../implementations/function_pointer.cpp"

L_.str1:                                ## @.str1
	.asciz	"bins > 0"

L_.str2:                                ## @.str2
	.asciz	"function != NULL"


.subsections_via_symbols
