// the functions we want to benchmark
#include "math_functions.hpp"
//#include "math_functors.hpp"
//#include "math_templated_functors.hpp"
// the different implementations of simpson_integration
#include "implementations/hard_coded.cpp"
#include "implementations/function_pointer.cpp"
//#include "implementations/templated_functor.hpp"
//#include "implementations/virtual_functor.cpp"

int main () {
	int a = 0;
	int b = 1;
	int bins = 1;
	//volatile auto result_hard_coded = 
	//	simpson_integration_f1(a, b, bins)
		// simpson_integration_f2(a, b, bins)
		// simpson_integration_f3(a, b, bins)
		// simpson_integration_f4(a, b, bins)
		// simpson_integration_f5(a, b, bins)
		//simpson_integration_f6(a, b, bins)
	;
	 volatile auto result_function_pointer = 
		simpson_integration(a, b, bins, &f1)
		// simpson_integration(a, b, bins, &f2)
		// simpson_integration(a, b, bins, &f3)
		// simpson_integration(a, b, bins, &f4)
		// simpson_integration(a, b, bins, &f5)
		// simpson_integration(a, b, bins, &f6)
	;
	// volatile auto result_virtual_functor = 
		// simpson_integration_virtual(a, b, bins, Functors::f1())
		// simpson_integration_virtual(a, b, bins, Functors::f2())
		// simpson_integration_virtual(a, b, bins, Functors::f3())
		// simpson_integration_virtual(a, b, bins, Functors::f4())
		// simpson_integration_virtual(a, b, bins, Functors::f5())
		// simpson_integration_virtual(a, b, bins, Functors::f6())
	;
	//volatile auto result_templated_functor = 
		// simpson_integration_templated(a, b, bins, TemplatedFunctors::mathconst<double, double>(0))
		// simpson_integration_templated(a, b, bins, TemplatedFunctors::mathconst<double, double>(1))
		// simpson_integration_templated(a, b, bins, TemplatedFunctors::pow<double, double, 1>())
		// simpson_integration_templated(a, b, bins, TemplatedFunctors::pow<double, double, 2>	())
		// simpson_integration_templated(a, b, bins, TemplatedFunctors::sin_lambda_x<double, double>(1.))
		// simpson_integration_templated(a, b, bins, TemplatedFunctors::sin_lambda_x<double, double>(5.))
	;
		
		
		
}