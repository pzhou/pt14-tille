#ifndef GENOME_HPP
	#define GENOME_HPP

// TODO: typedef that refers to a different class typedef
// TODO: add a list of original commentary from the assistant

// corrections
//	variable static but the related method was not

#include <bitset>
 
class Genome {
private:
	typedef num_genes_t unsigned short int;

	static const num_genes_t num_genes = 32;

	// specifies when the effect of a mutation will be present
	//	data should be declared protected such that subclasses
	//	can implement different mutate methods
	std::bitset<num_genes> data;

	// mutation rate (number of randomly toogled genes at birth)
	static mutationRate_t mutation_rate;
public:
	typedef unsigned short mutationRate_t;

	mutationRate_t getMutationRate();

	Genome();

	~Genome(); //if you declare it, you will need to implement it

	// should be virtual such that subclasses can implement 
	//	different mutate methods
  	virtual void mutate();

	// set the mutation rate
	static void setMutationRate(mutationRate_t rate); //this need to be static as it sets a static variable
};
#endif