//include guards!

//might be useful to wrap the animal and genome classes in a common
//namespace

// corrections:
// missing const
// missing underscore in naming
// missing const functions

#include "Genome.hpp"

class Animal {
private:
	typedef unsigned short age_t;
	typedef size_t allowed_mutation_threshold_t;
	typedef double prop_pregnant_t;

	// the age of this animal
	age_t age;

	// the genome of this animal
	const Genome genome;

	// the age when an animal is able to born children
	static const age_t age_to_adulthood;

	// maximum age an animal can reach
	static const age_t max_age;

	// whether this animal is pregnant
	bool pregnant;

	// number of allowed mutations until this animal dies
	static const Genome::num_genes allowed_mutation_threshold;

	// propability that an animal gets pregnant
	static const prop_pregnant_t prop_pregnant;
public:
	//---------getters-------------//
	// get the age of this animal
	age_t getAge() const;

	// is this animal still alive?
	bool isAlive() const;

	// is this animal pregnant?
	bool isPregnant() const;

	// add some additional properties for statistics & debugging here
	// ...

	// get the maximum age this animal can reach
	age_t getMaximumAge() const;

	static prop_pregnant_t getPropPregnant();

	//---------operations----------//
	// age this animal
	//	POST: same state as if ageIt(1) was called
  	Animal& operator++ (); //why not just use ageIt(1) then?

	// age this animal by num
	void ageIt(age_t num);

	// born a child
	//	PRE: 	animal is pregnant
	//	POST: 	returned animal was initiated with a mutation of 
	//				this animals genome
	Animal giveBirth() const;

	// set number of allowed mutations until this animal dies
	static void setAllowedMutationThreshold(allowed_mutation_threshold_t threshold);

};
