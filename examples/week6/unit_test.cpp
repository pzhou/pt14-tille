#include <iostream>
#include <iterator>
#include <cassert>
#include <limits>

int maxsum(const int vec[], const std::size_t n)
{
  int max_sums(0), max_sum(0);
  for(unsigned i = 0; i < n; ++i){
    max_sums = std::max(vec[i],vec[i]+max_sums);
    max_sum = std::max(max_sum,max_sums);
  }
  return max_sum;
}

struct test_maxsum
{
  static void empty_set()
  {
    const int vec[0]{};
    assert(maxsum(vec,0) == 0);
  }

  static void negative_numbers()
  {
    const int vec[3]{-1,-8,-4};
    assert(maxsum(vec,3) == 0);
  }

  static void positive_numbers()
  {
    const int vec[3]{1,8,4};
    assert(maxsum(vec,3) == 13);
  }

  static void mixed_numbers()
  {
    const int vec[3]{-1,8,-4};
    assert(maxsum(vec,3) == 8);
  }

  static void overflow()
  {
    const int a(std::numeric_limits<int>::max());
    const int vec[2]{a,a};
    assert(maxsum(vec,2) == a);
  }
};

int main()
{
  test_maxsum::empty_set();
  test_maxsum::negative_numbers();
  test_maxsum::positive_numbers();
  test_maxsum::mixed_numbers();
  test_maxsum::overflow();

  return 0;
}
