#include <iostream>
#include <limits>
#include "array.hpp"

// the general traits type:
template <class T>
struct average_type {
  typedef T type;
};
// the special case for int
template<>
struct average_type <int> {
typedef double type;};
// ... should be repeated for all integer types

template <class T>
typename average_type<T>::type
average(const Array<T>& v) {
 typename average_type<T>::type sum=0;
  for (int n=0;n<v.size();++n)
    sum += v[n];
  return sum/v.size();
}


int main() {
  std::size_t n=10;
  Array<double> v1(n,0);

  for(int i=0;i<n;++i) v1[i]=i;
  
  std::cout << "Average of double array: " << average(v1) << std::endl;

  Array<int> v2(n,0);
  
  for(int i=0;i<n;++i) v2[i]=i;
  
  std::cout << "Average of int array: " << average(v2) << std::endl;

  Array<short> v3(n,0);
  
  for(int i=0;i<n;++i) v3[i]=i;
  
  // no specialization implemented for short
  std::cout << "Average of short array: " << average(v3) << std::endl;

  return 0;
}
