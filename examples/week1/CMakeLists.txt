cmake_minimum_required(VERSION 2.8)
 
project(PT2014)

# compile the programs from sourse
add_executable(hello hello.cpp)
add_executable(add add.cpp)
add_executable(loop loop.cpp)
add_executable(pointer pointer.cpp)
add_executable(pointer2 pointer2.cpp)
add_executable(pointer3 pointer3.cpp)
add_executable(swap swapfixed.cpp)

