import numpy as np
import subprocess
import os
import sys
import matplotlib.pyplot as plt
from StringIO import StringIO

executable = 'penna_sim'

# check existence of the executable
if not os.access(executable,os.F_OK):
  print "Error: executable",executable,"was not found in the current directory."
  print "Use the sources in exercises/ex09/solution/penna/ to build it and copy it to the current directory (or create a link)."
  sys.exit()
# check the permission for execution of the executable
if not os.access(executable,os.X_OK):
  print "Error: executable",executable,"does not have permission to be executed."
  print "On Unix system use the 'chmod' command to set it."
  sys.exit()

TIME=700
#M=1
#T=3
R=7
P=.5
NMAX=200000
N0=NMAX/10
NMEAS=TIME
M1=200
M2=400
r1=0.05
a1=0
r2=0.11
a2=0

plt.title('Penna simulation with Fishing')
plt.xlabel('time')
plt.ylabel('Population')

for Ms in range(1,4):
    for Ts in range(2,5):
        print 'Running: M = ' + str(Ms) + ', T = ' +str(Ts)
        
        arguments = ['./'+executable,str(TIME),str(Ms),str(Ts),str(R),str(P),str(NMAX),str(N0),str(NMEAS),str(M1),str(M2),str(r1),str(a1),str(r2),str(a2)]

        penna_run = subprocess.Popen( arguments, stdout=subprocess.PIPE )
        penna_run.wait()

        results = penna_run.communicate()[0]

        data = np.loadtxt( StringIO(results) )

        plt.plot( data[4:,0] , data[4:,1] , label='M = '+str(Ms)+', T = '+str(Ts) )
        print 'Done'

plt.legend(loc='best')
plt.show()
