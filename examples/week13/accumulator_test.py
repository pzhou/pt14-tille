# Programming Techniques for Scientific Simulations, HS 2014
# Week 13

from accumulator import Accumulator

# for int
a = Accumulator()
a<<1
a<<2
a<<2
a<<3
a<<4
a<<5
print "int version:",a

# for float
a.reset()
a<<1.
a<<2.
a<<2.
a<<3.
a<<4.
a<<5.
print "float version:",a
